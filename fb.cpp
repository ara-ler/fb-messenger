#include <QApplication>
#include <QWebView>
#include <QObject>
#include <QMovie>
#include <QThread>
#include <QSystemTrayIcon>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QDir>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

QApplication *app;
QWebView *wview;
QSystemTrayIcon *trayIcon;
QIcon defIcon;
QIcon curIcon;
QMovie *gif;
QRegularExpression rgx("[0-9]{1,2}");
QRegularExpressionMatch match;

static void unix_sig_handler(int signum)
{
	printf("Caught signal %d\n", signum);
    switch (signum) {
        case SIGUSR2:
			wview->reload();
            break;
        case SIGTERM:
        case SIGUSR1:
            app->exit();
            break;
        default:
            return;
    }

}

static void iconUpdate()
{
	trayIcon->setToolTip(wview->title());
	match = rgx.match(wview->title());
	if (match.hasMatch()) {
		curIcon.addPixmap(gif->currentPixmap());
		trayIcon->setIcon(curIcon);
	}
	else {
		trayIcon->setIcon(defIcon);
	}
}

int main(int argc, char** argv)
{
	app = new QApplication(argc, argv);
	QWebSettings* settings = QWebSettings::globalSettings();

	if (signal(SIGUSR1, unix_sig_handler) == SIG_ERR)
		printf("Cannot install SIGUSR1 handler!\n");
	if (signal(SIGTERM, unix_sig_handler) == SIG_ERR)
		printf("Cannot install SIGTERM handler!\n");
	if (signal(SIGUSR2, unix_sig_handler) == SIG_ERR)
		printf("Cannot install SIGUSR2 handler!\n");

	defIcon = (QIcon(":/fb.png"));
	trayIcon = new QSystemTrayIcon(defIcon);
	trayIcon->show();
	printf("Adding tray icon\n");
	gif = new QMovie(":/fb.gif");
	gif->start();

	QObject::connect(gif, &QMovie::frameChanged, iconUpdate);

	settings->setAttribute(QWebSettings::LocalStorageEnabled, true);
	settings->setAttribute(QWebSettings::PrivateBrowsingEnabled,false);
	settings->setOfflineStoragePath(QDir::homePath() + "/.fb");
	settings->enablePersistentStorage(QDir::homePath() + "/.fb");

	wview = new QWebView(); 
	wview->setTextSizeMultiplier(0.8);

	if (argc > 1) 
		wview->setUrl(QUrl(argv[1]));
	else
		wview->setUrl(QUrl("https://messenger.com"));

	wview->show();
	app->exec();
	printf("Quit!\n");
	return 0;
}

